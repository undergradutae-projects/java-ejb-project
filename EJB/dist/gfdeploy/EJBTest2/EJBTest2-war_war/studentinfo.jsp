<%--    Document   : studentinfo
    Created on : Sep 30, 2015, 4:14:26 PM
    Author     : Hasan Mahmud
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Information</title>
    </head>
    <body>
        <h1>Student Information</h1>
        <form> 
        <table method="POST" action="/StudentServlet">
            <tr>
                <td>Student ID</td>
                <td><input type="text" name="studentId" value="${student.studentId}"/></td>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name" value="${student.name}"/></td>
            </tr>
            <tr>
                <td>Department</td>
                <td><input type="text" name="department" value="${student.department}"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="submit" name="action" value="Add"/>
                    <input type="submit" name="action" value="Edit"/>
                    <input type="submit" name="action" value="Delete"/>
                    <input type="submit" name="action" value="Search"/>
                </td>
            </tr>
        </table>
        </form>
        <br>
        <table border="1">
            <th>ID</th>
            <th>Name</th>
            <th>Department</th>
            <c:forEach items="${allStudents}" var="stud">
                <tr>
                    <td>${stud.studentId}</td>
                    <td>${stud.name}</td>
                    <td>${stud.department}</td>
                </tr>
                
            </c:forEach>
        </table>
    </body>
</html>
